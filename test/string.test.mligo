#import "../lib/string.mligo" "String_helper"

let test_repeat =
  assert ("x_x_" = String_helper.repeat("x_", 2n))
